package Dao;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Data {

    private String hostnome;
    private int port;
    private String database;
    private String username;
    private String password;

    private Connection connection;

    public Data() {
        try {
            hostnome = "localhost";
            port = 3306;
            database = "cruddibb";
            username = "root";
            password = "";

            String url = "jbc:mysql://" + hostnome + ":" + port + "/" + database;
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connection connection = (Connection) DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            System.out.println(" Erro de conexão " + ex.getMessage());

        }
    }

    public Connection GetConnection() {
        return this.connection;
    }

    public void CloseDataSource() {
        try {

            connection.close();
        } catch (Exception ex) {
            System.err.println("Erro ao desconectar");
        }
    }
}
